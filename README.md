#Project Frozen Fist
##VortexOps

![Author Untied States Department of Denfense](https://upload.wikimedia.org/wikipedia/commons/e/e0/United_States_Department_of_Defense_Seal.svg)

>###Project Objectives
>> The goal of project Frozen Fist is to design a military software application that can allow individuals to pilot armored assault vehicles as well as on-board AIs to complete the same job.  

Project Intern: [Evan Horter](http://evan.horter@smail.ramsussen.edu)

### Pushed to BitBucket repository - 2/17/2018
### Modified README on BitBucket - 2/26/18